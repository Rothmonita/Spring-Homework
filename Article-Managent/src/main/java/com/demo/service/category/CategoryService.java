package com.demo.service.category;

import java.util.List;

import com.demo.model.Category;

public interface CategoryService {
	List<Category> findAll();
	Category findOne(int id);
}
