package com.demo.service.article;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.model.Article;
import com.demo.repository.article.ArticleRepository;
import com.github.javafaker.Faker;

@Service
public class ArticleServiceImp implements ArticleService{//business logic
	private ArticleRepository articleRepository;
	@Autowired
	public ArticleServiceImp (ArticleRepository articleRepository) {
		this.articleRepository=articleRepository;
	}
	

	@Override
	public void add(Article article) {
		articleRepository.add(article);
		
	}

	@Override
	public List<Article> findAll() {
		return articleRepository.findAll();
	}

	@Override
	public Article findOne(int id) {
		return articleRepository.findOne(id);
	}


	@Override
	public void delete(int id) {
		articleRepository.delete(id);
		
	}


	@Override
	public Article view(int id) {
		return articleRepository.view(id);
	}


	@Override
	public void update(Article article) {
		articleRepository.update(article);
		
	}


	@Override
	public int generateId() {
		return articleRepository.generateId();
	}
	
}
