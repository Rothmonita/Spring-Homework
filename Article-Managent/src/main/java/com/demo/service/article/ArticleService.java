package com.demo.service.article;

import java.util.List;

import com.demo.model.Article;

public interface ArticleService {
	void add(Article article);
	List<Article> findAll();
	Article findOne(int id);
	void delete(int id);
	Article view(int id);
	void update(Article article);
	int generateId();
}
