package com.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.hibernate.validator.internal.util.privilegedactions.NewInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.demo.model.Article;
import com.demo.service.article.ArticleService;
import com.demo.service.category.CategoryService;

import ch.qos.logback.classic.ViewStatusMessagesServlet;


@Controller
@PropertySource("classpath:ams.properties")
public class ArticleController {
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;

	@Value("${file.upload.server.path}")
	private String folderPath;
	@Autowired
	public ArticleController(ArticleService articleService) {
		this.articleService = articleService;
	}

	// public void setArticleService(ArticleService articleService) {
	// this.articleService = articleService;
	// }

	@GetMapping("/article")
	public String Ariticle(ModelMap m) {
		List<Article> articles = articleService.findAll();
		m.addAttribute("articles", articles);
		return "article";
	}
	@GetMapping("/add-new")
	public String addNew(ModelMap m,Article article) {
		m.addAttribute("article",article);
		article.setCreatedDate(new Date().toString());
		m.addAttribute("idd", articleService.findAll().size()+1);
		m.addAttribute("categories",categoryService.findAll());
		m.addAttribute("formAdd", true);
		return "add-new";
	}
	
	@PostMapping("/add-new")
	public String saveArticle(@RequestParam("image") MultipartFile file, @Valid @ModelAttribute Article article,
			BindingResult result, ModelMap m) {
		String uuid = UUID.randomUUID() + "."
				+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
		if (result.hasErrors()) {
			m.addAttribute("article", article);
			m.addAttribute("categories",categoryService.findAll());
			m.addAttribute("formAdd", true);
			return "add-new";
		}
		if (file.isEmpty()) {
			return "add-new";
		} else {
			try {
				
				Files.copy(file.getInputStream(), Paths.get(folderPath, uuid));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		article.setthumbnail("/image/" +uuid);
		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		article.setCreatedDate(new Date().toString());
		articleService.add(article);
		return "redirect:/article";

	}
	
	/*@PostMapping("/add-new")
	public String saveArticle( @Valid @ModelAttribute Article article,
			BindingResult result, ModelMap m) {
		if (result.hasErrors()) {
			m.addAttribute("article", article);
			m.addAttribute("categories",categoryService.findAll());
			m.addAttribute("formAdd", true);
			return "add-new";
		}
		article.setCreatedDate(new Date().toString());
		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		articleService.add(article);
		return "redirect:/article";

	}*/

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		articleService.delete(id);
		return "redirect:/article";
	}

	@GetMapping("/view/{id}")
	public String view(ModelMap m, @PathVariable("id") int id) {
		m.addAttribute("article", articleService.view(id));
		return "view";
	}

	@GetMapping("/update/{id}")
	public String update(ModelMap m, @PathVariable("id") int id) {
		m.addAttribute("article", articleService.findOne(id));
		m.addAttribute("categories",categoryService.findAll());
		return "update";
	}
	
	@PostMapping("/update")
	public String updateArticle(@ModelAttribute Article article,ModelMap m) {
//		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		articleService.update(article);
		return "redirect:/article";
	}

}
